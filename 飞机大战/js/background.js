function loadBg(bgURL,bgCanvas,moveBg){
  let cvsCtx = bgCanvas.getContext('2d');
  let width = bgCanvas.width;
  let height = bgCanvas.height;

  let img = new Image();
  img.src = bgURL;
  img.onload = function(){
    cvsCtx.drawImage(img, 0, 0);
    //cvsCtx.drawImage(img, 0, -height);
    if(moveBg){
      moveBg(img);
    }
  }
}
let y = 0;
//背景移动
function bgMove(bgCanvas,img,speed){
  let cvsCtx = bgCanvas.getContext('2d');
  let width = bgCanvas.width;
  let height = bgCanvas.height;

  //记录状态
  cvsCtx.save();
  cvsCtx.clearRect(0, 0, width, height);
  cvsCtx.translate(0, y);
  cvsCtx.drawImage(img, 0, 0);
  cvsCtx.drawImage(img, 0, -height);
  y += speed;
  if(y>=height){
    y = 0;
  }
  cvsCtx.restore();
}