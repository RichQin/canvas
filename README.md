# canvas

### 介绍
canvas的一些基础学习，以及一些小demo

预览：http://richqin.gitee.io/canvas/  

### canvas画布笔记
#### 按点画线
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Canvas</title>
</head>
<body onload="draw()" style="text-align: center;">
  <canvas id="canvas" width="800px" height="400px" style="border:1px solid #000;"></canvas>
  <script>
    function draw(){
      let canvas = document.getElementById("canvas");
      if(canvas.getContext){
        //创建context对象
        let cvsCtx = canvas.getContext("2d");
        //fillRect(x,y,w,h)
       /*  cvsCtx.fillStyle = 'pink';
        cvsCtx.fillRect(0, 0, 200, 200); */
        //填充颜色
        cvsCtx.fillStyle = '#000';
        //起始点
        cvsCtx.moveTo(0,0);
        //画线
        cvsCtx.lineTo(100,0);
        cvsCtx.lineTo(0,100);
        //填充
        cvsCtx.fill();
        //描边颜色
        cvsCtx.strokeStyle = '#000';
        cvsCtx.moveTo(10,110);
        cvsCtx.lineTo(110,110);
        cvsCtx.lineTo(110,110);
        cvsCtx.lineTo(110,10);
        cvsCtx.lineTo(10,110);
        //描边结束
        cvsCtx.stroke();
      }
    }
  </script>
</body>
</html>
```
#### 画圆
```javascript
function draw(){
      let canvas = document.getElementById("canvas");
      if(canvas.getContext){
        //创建contect对象
        let cvsCtx = canvas.getContext("2d");
        //画圆(圆心x,y,半径,开始角度,结束角度,顺时针false/逆时针true)
        cvsCtx.strokeStyle = '#000';
        //线宽
        //cvsCtx.lineWidth = 10;
        //画笑脸
        cvsCtx.beginPath();
        cvsCtx.arc(400, 200, 150, 0, 2*Math.PI, false);
        cvsCtx.moveTo(500, 200);
        cvsCtx.arc(400, 200, 100, 0, Math.PI, false);
        cvsCtx.closePath();
        cvsCtx.moveTo(370, 120);
        cvsCtx.arc(350, 120, 20, 0, 2*Math.PI, false);
        cvsCtx.moveTo(470, 120);
        cvsCtx.arc(450, 120, 20, 0, 2*Math.PI, false);
        cvsCtx.stroke();
      }
    }
```

#### 渐变
```javascript
//参数1：0-1，表示颜色所在位置
//参数2: 颜色 white #fff #ffffff rgb(255,255,255)
//创建一个线性渐变对象(x,y,w,h)
let grd = cvsCtx.createLinearGradient(0,0,0,200);
//创建径向渐变对象 (x1,y1,r1,x2,y2,r2)
//var rad = cvsCtx.createRadialGradient(120,120,20,120,120,50);
//添加颜色
grd.addColorStop(0,"#000");
grd.addColorStop(0.5,"#ccc");
grd.addColorStop(1,"#fff");
cvsCtx.fillStyle = grd;
cvsCtx.fillRect(0,0,200,200);        
```

#### 绘制图像
```javascript
//绘制图像
let img=new Image()
img.src="info.png"
//等待图片加载完
img.onload = function(){
//drawImage(img,x,y,width,height,从x开始截取,y,w,h)
  cvsCtx.drawImage(this,0,0);
}     
```

#### 绘制文字
```javascript
//绘制文字
//cvsCtx.font = 'style, weight, size, family';
cvsCtx.font = '160px italic';
//阴影
cvsCtx.shadowColor = '#ccc';
//阴影模糊度，偏移
cvsCtx.shadowBlur = 10;
cvsCtx.shadowOffsetX = 20;
cvsCtx.shadowOffsetY = 20;

//cvsCtx.fillText/strokeText('text', x, y);
cvsCtx.fillText('RichQin', 20, 150);    
```

#### clip()切图

#### 图像组合   
`cvsCtx.globalCompositeOperation = "destination-out";`  
>目标图像：画布已存在图像 destination  
>源图像：即将画上去的图像 source 
- source-over	默认。在目标图像上显示源图像。
- destination-over	在源图像上显示目标图像。
- source-atop / destination-atop	在目标图像上显示源图像以及目标图像。源图像非重合部分是不可见。
- source-in /	destination-in 显示重合部分源图像
- source-out / destination-out	显示不重合的源图像，目标图像是透明的。
- lighter	显示源图像 + 目标图像。
- copy	显示源图像。忽略目标图像。
- xor	使用异或操作对源图像与目标图像进行组合。(重合为0，不重合为1)

#### save()和restore()  
>保存数据和使用数据`cvsCtx.save();cvsCtx.restore();`  
>使用栈的存取方式，先进后出，先使用最后保存的数据

#### 关键帧运动  
```javascript
let x = 0,y = 0,width = 50,height = 50;
cvsCtx.beginPath();
cvsCtx.fillStyle = "pink";
cvsCtx.fillRect(x, y, width, height);
let speedX = 2,speedY = 2;
// 1 请求关键帧动画（自动规划最优动画，不会失帧）
function move(){
  //清除画布
  cvsCtx.clearRect(x, y, canvas.width, canvas.height);
  //更新位置
  x += speedX;
  if(x > canvas.width-width || x < 0){
    speedX *= -1;
  }
  y += speedY;
  if(y > canvas.height-height || y < 0){
    speedY *= -1;
  }
  //绘制图像
  cvsCtx.fillRect(x, y, width, height);
  //请求关键帧动画
  window.requestAnimationFrame(move);
}
  move();
```
